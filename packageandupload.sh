#Generating distribution archives

python3 -m pip install --user --upgrade setuptools wheel
python3 setup.py sdist bdist_wheel 


# Upload distribution archive

python3 -m pip install --user --upgrade twine
ls
python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

# Delete all install files generated

#rm -rf build/ dist/ gitcreateusers.egg-info/ 

