# Script permettant de creer des utilisateur avec API gitlab
# Fournir le token en ligne de commande
# Fournir un fichier avec les donnees des user
    # username
    # email
    # name
    # password
# Question 
    # Comment passer les arguments en LDC: argparse
    # Quel type de fichier il faudra mettre les users: yaml
    # Comment generer un mot de pass automatiquement: random

import argparse
import gitlab
import random
import string
import logging
import yaml 

from logformat import CustomFormatter


def connectGitServer(serveradress, token, logger):
        '''Fonction permettant de se connecter au server gitlab
        @param serveradress: addresse du server gitlab
        @param token: token d'authentification au serveur
        '''
        logger.info(f"Adresse du server: {serveradress}")
        logger.info("Initialisation de la connexion au server")
        try:
            gl = gitlab.Gitlab(serveradress, private_token=token)
            gl.auth()
        except Exception as e:
            logger.error(f"Erreur lors de la connexion au server {serveradress}: {e}")
        
        return gl


class CreateUsers:
    '''Classe permettant de creer des utilisateurs grace a l'API gitlab
    '''

    def __init__(self, filename, serveraddress, token, logger):
        '''
        @param filename: fichier de configuration yaml contenant les utilisateurs a creer
        @param serveradress: Adresse du server gitlab
        @param token: Token d'authentification au server gitlab
        @param logger: logger
        '''
        self.filename = filename
        self.serveraddress = serveraddress
        self.token = token
        self.logger = logger

    # Lecture du fichier yaml contenant les utlisateurs a creer
    def readYaml (self):
        ''' Read yaml file
        @return: dict
        '''
        self.logger.info("Lecture du fichier de configuration")
        with open(self.filename, 'r') as stream:
            data_loaded = yaml.safe_load(stream)
    
        return data_loaded
    
    #Creation des utilisateurs avec un mot de pass predefini
    def user_create(self, users_data):
        '''
        @param (users_data) : dictionnaire de donnee 
        '''
        # genere un mot de passe pour chaque utilistateur
        for data in users_data.values():
            data['password'] = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))

        # cree les utilisateurs
        gitCl = connectGitServer(self.serveraddress, self.token, self.logger)
        for data in users_data.values():
            try:
                gitCl.users.create(data)
                self.logger.info(f"l'utilisateur {data['name']} est cree")
            except Exception as e:
                self.logger.error(f"Erreur lors de la creation de l'utilisateur {data['username']}: {e}")





def main():
    # create logger with 'spam_application'
    logger = logging.getLogger("GITCREATEUSERS")
    logger.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    ch.setFormatter(CustomFormatter())

    logger.addHandler(ch)

    parser = argparse.ArgumentParser(description="Help menu")
    parser.add_argument("usersfile", help="Yaml onfiguration file with users to be created", type=str)
    parser.add_argument("serveraddress", help="gitlab server address", type=str)
    parser.add_argument("token", help="gitlab server authentication token", type=str)
    
    
    args = parser.parse_args()

    crUsers = CreateUsers(args.usersfile, args.serveraddress, args.token, logger)
    usersData = crUsers.readYaml()
    crUsers.user_create(usersData)

if __name__ == "__main__":
    main()