# Ce script permet de créer plusieurs utilisateurs grâce à l'api gitlab


## Prérequis

- Installer python 3
- Installer pip3
- Installer les différents librairies python listée dans requirement.txt avec: pip install -r requirement.txt
- Générer un token d'authentification sur le serveur
- Remplir le fichier de configuration yaml avec les données utilisateurs

## Commande pour lancer le script

``` 
python3  create_users.py <fichierconf.yaml> <adresse_du_serveur_gitlab> <token_d'authentification> 
``` 

## Exemple

- fichier de configuration: users.yaml
- token d'authentification: 6yxoBBdwiNMCeUpo1Tmj
- Adresse du serveur gitlab: www.gitlab-adress.com

``` 
python3  create_users.py donnee_users.yaml www.gitlab-adress.com 6yxoBBdwiNMCeUpo1Tmj
``` 
